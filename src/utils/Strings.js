import {Text} from "react-native";
import React from "react";

const STRINGS = {
    
    //========================================================
    //===================Class Detail Page====================
    //========================================================
    CLASS_DETAILS_TOP_TEXT:'Class Details',
    CLASS_DETAILS_CLASS_ID:'class ID : 216778',
    CLASS_DETAILS_MEMBERS_TEXT:'Members',
    CLASS_DETAILS_RECOMMEND_TEXT:'RECOMMEND',
    CLASS_DETAILS_SEEALL_TEXT:'See all',
    KINGDOM_TITLE:'Giants Kingdom. This image is selling well – which is gratifying as it is the product of my signature style of elephant photography',

    //==========================================================
    //===================Profile Detail Page====================
    //==========================================================
    CLIENT_HEADER_TITLE:'Client',
    CLIENT_BASIC_INFO_TEXT:'Basic Informations',
    CLIENT_PROFILE_TEXT:'Profile',
    CLIENT_RELATIVES_TEXT:'Relatives',
    CLIENT_COMPLETION_TEXT:'Completion Status',
    CLIENT_TOTAL_RELATIVES_ADDED:'Total Relatives added',
    CLIENT_MODIFIED_ON:'Modified on',
    CLIENT_ADD_RELATIVES:'+  Add Relatives',
    CLIENT_CASES_TEXT:'Cases',
    CLIENT_OPEN_TEXT:'Open',
    CLIENT_DECIDED_TEXT:'Decided',
    CLIENT_RELATED_TEXT:'Related',
    CLIENT_DOCUMENT_TEXT:'Documents',

   
};
export default STRINGS
