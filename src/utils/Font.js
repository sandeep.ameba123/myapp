export const FONT_FAMILY = {
    "Montserrat": "Montserrat-Regular",
    "MontserratBold": "Montserrat-Bold",
    "Roboto": "Roboto-Regular",
    "RobotoBold": "Roboto-Bold"
};
