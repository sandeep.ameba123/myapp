import styled from "styled-components";
import {widthPercentageToDP as wp} from "react-native-responsive-screen";
import ShadowView from "react-native-simple-shadow-view";
import {SafeAreaView} from 'react-native'
import COLORS from "./Colors";

const MainContainer = styled.View`
flex: 1;
backgroundColor: ${COLORS.white_color};
alignItems: center;

`;
const ScrollContainer = styled.ScrollView`
flex: 1;
backgroundColor:${COLORS.white_color};
`;

const ShadowViewContainer = styled(ShadowView)`
 shadowColor: ${COLORS.black_color};
 shadowOpacity: ${wp(0.05)};
 shadowRadius: ${wp(1)};
 borderRadius: ${wp(3)};
 shadowOffset: 0px 2px;
 backgroundColor: ${COLORS.white_color};
 marginBottom: ${wp('4%')};
`;
const NestedShadowViewContainer = styled(ShadowView)`
 shadowColor: ${COLORS.black_color};
 shadowOpacity: ${wp(0.09)};
 shadowRadius: ${wp(1)};
 borderRadius: ${wp(3)};
 shadowOffset: 0px 2px;
 backgroundColor: ${COLORS.white_color};
 marginBottom: ${wp('4%')};
`;
const BorderViewContainer = styled.View`
 borderColor:${COLORS.light_grey};
 borderWidth: 1 ;
 borderRadius:10;
 paddingVertical:4;
`;

const SafeAreaViewContainer = styled(SafeAreaView).attrs(() => ({
    forceInset: {top: 'never'}
}))`
flex:1
`;

export {
    MainContainer,
    ScrollContainer,
    ShadowViewContainer,
    SafeAreaViewContainer,
    NestedShadowViewContainer,
    BorderViewContainer
}
