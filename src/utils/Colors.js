import { Colors} from 'react-native/Libraries/NewAppScreen';

const COLORS = {
    app_theme_color: '#4582E5',
    backGround_color: "#E5E5E5",
    white_color: "#ffffff",
    black_color: "#000000",
    light_grey: "#E2E2E2",
    transparent: 'transparent',
    normal_grey:"grey",
    dark_grey:'darkgrey'

};
export default COLORS
