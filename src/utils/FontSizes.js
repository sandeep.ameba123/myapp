import {widthPercentageToDP as wp} from 'react-native-responsive-screen'

export const FONT = {
    TextHero: wp(10),
    TextLarge: wp(8),
    TextNormal: wp(6),
    TextMedium: wp(4.5),
    TextSmall: wp(4),
    TextSmall_2: wp(3.5),
    TextExtraSmall: wp(3),
    TextXExtraSmall: wp(2.5),
};
