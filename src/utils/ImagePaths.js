const ICONS = {
        
    BACKGROUND_IMG: require('../../assets/images/background.png'),
    CLASS_BG_IMG: require('../../assets/images/classbg.png'),
    USER_IMG: require('../../assets/images/user.png'),
    FLAG_ICON: require('../../assets/images/flag.png'),
    KINGDOM_IMG: require('../../assets/images/kingdom.png'),

};

const HEADER_ICONS = {
   
};

export {
    ICONS,
    HEADER_ICONS
}
