import React from 'react';
import styled from 'styled-components';
import COLORS from "../../utils/Colors";
import { FONT } from "../../utils/FontSizes";
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen'
import { StyleSheet } from 'react-native';
const styles = StyleSheet.create({

    mainContainerStyle: {
        flex: 1,
    },
    mainView: {
        padding:wp('3%'),
        marginTop:Platform.OS === 'ios' ? 0 : wp('25%') ,
        flex:1,
    },
    image: {
        resizeMode: "cover",
        height: hp('25%'),
        width: '100%',
        flex:1
    },
    userImage: {
        resizeMode: "cover",
        height: wp('16%'),
        width: wp('16%'),
        borderRadius: wp('16%') / 2,
        borderWidth: 0.5,
        borderColor: COLORS.white_color,
        alignSelf:"center",
        top:-wp('8%')
    },
    flagIcon: {
        resizeMode: "cover",
        height: wp('8%'),
        width: wp('8%'),
        borderRadius: wp('8%') / 2,
        position:"relative",
        alignSelf:"center",
        top:-wp('3%')
        
    },
    profileContainer:{
        width: wp('45%'),
        backgroundColor:Platform.OS === 'ios' ? COLORS.backGround_color : COLORS.white_color ,
        justifyContent: "center",
        padding:wp('2%'),
        margin:wp('1%')
    },
    mainHeadingStyle: {
        fontWeight: "600",
        color: COLORS.black_color,
        alignSelf:"flex-start",
        fontSize:FONT.TextMedium,        
    },
    mediumTextStyle: {
        color: COLORS.black_color,
        fontSize:FONT.TextSmall,        
    },
    smallTextStyle: {
        color: COLORS.normal_grey,
        fontSize:FONT.TextExtraSmall,        
    },
    normalTextStyle: {
        color: COLORS.white_color,
        fontSize:FONT.TextMedium,        
    },
    addRelativeStyle: {
        color: "blue",
        fontSize:FONT.TextMedium,        
    },
    avatar: {                                                                                                                                                                                                        
        backgroundColor: 'lightgreen',                                                                                                                                                                                    
        resizeMode: "cover",
        height: wp('8%'),
        width: wp('8%'),
        borderRadius: wp('8%') / 2,
        flexDirection:"row"                                                                                                                                                                                                 
      },                                                                                                                                                                                                               
      badge: {                                                                                                                                                                                                         
         backgroundColor: 'pink',                                                                                                                                                                                      
         resizeMode: "cover",
         height: wp('8%'),
         width: wp('8%'),
         borderRadius: wp('8%') / 2,                                                                                                                                                                                                  
         left: 20,                                                                                                                                                                                                    
         top: 0,                                                                                                                                                                                                   
      },   
})

export default styles;