/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import React from "react";
import {
    View, Text, Image, Keyboard, StatusBar, FlatList,
    TouchableWithoutFeedback, Platform, ImageBackground, ScrollView
} from "react-native";
import styles from "./styles";
import BaseClass from "../../utils/BaseClass";
import STRINGS from '../../utils/Strings'
import COLORS from "../../utils/Colors";
import { FONT } from "../../utils/FontSizes";
import {
    SafeAreaViewContainer, MainContainer, ShadowViewContainer,
    ScrollContainer
} from "../../utils/BaseStyle";
import { ICONS } from "../../utils/ImagePaths";
// import FullScreen from "../../components/FullScreenView/FullScreen";
import { Spacer } from '../../components/Spacer/SpacerView'
// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import { Header } from 'react-native-elements';
import { widthPercentageToDP as wp } from 'react-native-responsive-screen'
import Icon from 'react-native-vector-icons/AntDesign';
Icon.loadFont()
import * as Progress from 'react-native-progress';
const leftIcon = <Icon name="left" size={25} color="#FFF" />;
const rightIcon = <Icon name="menuunfold" size={25} color="#FFF" />;
const DismissKeyboard = ({ children }) => (
    <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
        {children}
    </TouchableWithoutFeedback>
);
class ProfileDetail extends BaseClass {

    constructor(props) {
        super(props);
        this.state = {
            colorsList: [
                '#6059FF',
                '#5CCFFE',
                '#F9D504',
            ],
            ListItems: [
                { name: 'Open', counter: 4 },
                { name: 'Decided', counter: 4 },
                { name: 'Related', counter: 4 },
            ],
        }
    }
    /*
// =============================================================================================
// Render method for Header
// =============================================================================================
*/
    _renderHeader() {
        const { navigation } = this.props;
        return (
            <Header
                backgroundColor={COLORS.transparent}
                statusBarProps={{
                    translucent: true,
                    backgroundColor:"transparent",
                    barStyle: "light-content"
                }}
                leftComponent={(leftIcon)}
                centerComponent={{
                    text: STRINGS.CLIENT_HEADER_TITLE, style: {
                        color: COLORS.white_color,
                        fontSize: FONT.TextMedium,
                        fontWeight: "bold"
                    }
                }}
                rightComponent={(rightIcon)}
                containerStyle={{
                    borderBottomColor: 'transparent',
                    paddingTop: Platform.OS === 'ios' ? 0 : 25,
                    height: Platform.OS === 'ios' ? 50 : StatusBar.currentHeight + 60,
                }}
            />
        )
    }

    // -------------------------------------------------------


    // =============================================================================================
    // Render method for profile
    // =============================================================================================
    _renderProfileDetail() {
        return (
            <ShadowViewContainer style={{ backgroundColor: Platform.OS === 'ios' ? COLORS.backGround_color : COLORS.white_color }}>
                <View style={{
                    flexDirection: "row", borderBottomColor: COLORS.backGround_color,
                    borderBottomWidth: 1,
                    padding: wp('2%'),
                    margin: wp('2%'),
                    width: wp('90%')
                }}>
                    <View style={{ width: wp('70%') }}>
                        <Text style={styles.mediumTextStyle} >Micheal Henry</Text>
                        <Spacer space={1} />
                        <Text style={styles.smallTextStyle} >Micheal@gmail.com</Text>
                        <Spacer space={1} />
                        <Text style={styles.smallTextStyle} >93484489829</Text>
                        <Spacer space={1} />
                        <Text style={styles.smallTextStyle} >Civil Hospital</Text>
                        <Spacer space={1} />
                    </View>
                    <View>
                        <Image style={styles.userImage}
                            source={ICONS.USER_IMG} />
                        <Spacer
                            space={0.5} />
                        <Image style={styles.flagIcon}
                            source={ICONS.FLAG_ICON} />
                    </View>
                </View>
                <View style={{
                    flexDirection: "row",
                    justifyContent: "space-between",
                    padding: wp('2%'),
                    margin: wp('2%')
                }}>
                    <Text style={styles.mediumTextStyle} >Micheal Henry</Text>
                    <Text style={styles.mediumTextStyle} >Micheal@gmail.com</Text>
                </View>

            </ShadowViewContainer>
        )
    }
    // -------------------------------------------------------


    // =============================================================================================
    // Render method for Basic info
    // =============================================================================================
    _renderBasicInfo() {
        return (
            <View>
                <Text style={styles.mainHeadingStyle}>{STRINGS.CLIENT_BASIC_INFO_TEXT}</Text>
                <Spacer space={1.5} />
                <View style={{
                    flexDirection: "row",
                    justifyContent: "space-evenly",
                }}>
                    <ShadowViewContainer style={styles.profileContainer} >
                        <Text style={styles.mainHeadingStyle} >{STRINGS.CLIENT_PROFILE_TEXT}</Text>
                        <Spacer space={1.5} />
                        <Text style={styles.smallTextStyle} >{STRINGS.CLIENT_COMPLETION_TEXT}</Text>
                        <Spacer space={1.5} />
                        <Text style={styles.mainHeadingStyle} >60%</Text>
                        <Spacer space={1.5} />
                        {/* progress bar here */}
                        <Progress.Bar progress={0.6} width={wp('40%')} />
                        <Spacer space={1.5} />
                        <Text style={styles.smallTextStyle}>{STRINGS.CLIENT_MODIFIED_ON}</Text>
                        <Text style={styles.smallTextStyle}>"Jun 04, 2019"</Text>
                        <Spacer space={0.5} />
                    </ShadowViewContainer>
                    <Spacer row={2} />
                    <ShadowViewContainer style={styles.profileContainer} >
                        <Text style={styles.mainHeadingStyle}  >{STRINGS.CLIENT_RELATED_TEXT}</Text>
                        <Spacer space={1.5} />
                        <Text style={styles.smallTextStyle}>{STRINGS.CLIENT_TOTAL_RELATIVES_ADDED}</Text>
                        <Spacer space={1.5} />
                        <Text style={styles.mainHeadingStyle}  >04</Text>
                        <Spacer space={1.5} />
                        {/* images list here */}
                        <View style={{ width: wp('40%'), flexDirection: "row" }}>
                            <View style={styles.avatar}>
                                <View style={styles.badge} />
                            </View>
                            <View style={styles.avatar}>
                                <View style={styles.badge} />
                            </View>
                        </View>
                        <Spacer space={1.5} />
                        <Text style={styles.addRelativeStyle} >{STRINGS.CLIENT_ADD_RELATIVES}</Text>
                        <Spacer space={0.5} />
                    </ShadowViewContainer>
                </View>
            </View>
        )
    }

    // =============================================================================================
    // Render method for Listview
    // =============================================================================================


    _renderMyList = ({ item, index }) => (

        <View style={{
            backgroundColor: this.state.colorsList[index % this.state.colorsList.length],
            borderRadius: wp('2%'), padding: wp('5%'),
            width: wp('27%'),
            height: wp('25%')
        }} >
            <Text style={styles.normalTextStyle} >{item.name}</Text>
            <Spacer space={1} />
            <Text style={styles.normalTextStyle} >{item.counter}</Text>
        </View>

    );

    _itemSeparator = () => (<View style={{ width: wp('5%') }} />);

    // -------------------------------------------------------

    render() {
        const { ListItems } = this.state;
        return (
            // <SafeAreaViewContainer>

            <DismissKeyboard>
                <MainContainer style={styles.mainContainerStyle}>
                    <ImageBackground source={ICONS.BACKGROUND_IMG}
                        style={styles.image}
                        imageStyle={{
                            borderTopLeftRadius: wp('1%'),
                            borderTopRightRadius: wp('1%')
                        }}>
                        <SafeAreaViewContainer>
                            {this._renderHeader()}
                        </SafeAreaViewContainer>

                        <ScrollView>
                            <View style={styles.mainView}>
                                <Spacer
                                    space={2} />
                                {this._renderProfileDetail()}
                                <Spacer
                                    space={2} />
                                {this._renderBasicInfo()}
                                <Spacer
                                    space={2} />
                                <Text style={styles.mainHeadingStyle} >{STRINGS.CLIENT_CASES_TEXT}</Text>
                                <FlatList
                                    horizontal
                                    style={{ padding: wp('2%') }}
                                    data={ListItems}
                                    renderItem={this._renderMyList}
                                    ItemSeparatorComponent={this._itemSeparator}
                                    extraData={this.state.refresh}
                                    keyExtractor={(item, index) => index}
                                />
                                <Spacer
                                    space={2} />
                                <Text style={styles.mainHeadingStyle}>{STRINGS.CLIENT_DOCUMENT_TEXT}</Text>
                            </View>
                        </ScrollView>
                    </ImageBackground>
                </MainContainer>
            </DismissKeyboard>

            // </SafeAreaViewContainer>
        )
    }
}

export default ProfileDetail;