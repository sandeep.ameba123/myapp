/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import React from "react";
import {
    View, Text, Image, Keyboard, StatusBar, ImageBackground,
    TouchableOpacity, TouchableWithoutFeedback, Platform, ScrollView,
    FlatList
} from "react-native";
import styles from "./styles";
import BaseClass from "../../utils/BaseClass";
import STRINGS from '../../utils/Strings'
import COLORS from "../../utils/Colors";
import { FONT } from "../../utils/FontSizes";
import {
    SafeAreaViewContainer, MainContainer, ShadowViewContainer,
    ScrollContainer
} from "../../utils/BaseStyle";
import { ICONS } from "../../utils/ImagePaths";
import { Spacer } from '../../components/Spacer/SpacerView'
// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import { Header } from 'react-native-elements';
import { widthPercentageToDP as wp } from 'react-native-responsive-screen'
import Icon from 'react-native-vector-icons/AntDesign';
Icon.loadFont()

const DismissKeyboard = ({ children }) => (
    <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
        {children}
    </TouchableWithoutFeedback>
);
class ClassDetail extends BaseClass {

    constructor(props) {
        super(props);
        this.state = {
            colorsList: [
                '#6059FF',
                '#5CCFFE',
                '#F9D504',
            ],
            ListItems: [
                { name: 'Open', counter: 4 },
                { name: 'Decided', counter: 4 },
                { name: 'Related', counter: 4 },
            ],
        }
    }
    // =============================================================================================
    // Render method for Header
    // =============================================================================================
    _renderHeader() {
        return (
            <Header
                backgroundColor={COLORS.transparent}
                statusBarProps={{
                    translucent: true,
                    backgroundColor: "transparent",
                    barStyle: "dark-content"
                }}
                centerComponent={{
                    text: STRINGS.CLASS_DETAILS_TOP_TEXT, style: {
                        color: COLORS.white_color,
                        fontSize: FONT.TextNormal,
                        fontWeight: "bold",
                    }
                }}
                containerStyle={{
                    borderBottomColor: 'transparent',
                    paddingTop: Platform.OS === 'ios' ? 0 : 25,
                    height: Platform.OS === 'ios' ? 50 : StatusBar.currentHeight + 60,
                }}
            />
        )
    }

    // -------------------------------------------------------


    // =============================================================================================
    // Render method for members views
    // =============================================================================================
    _renderMembers() {
        const { ListItems } = this.state;
        return (
            <ShadowViewContainer>
                <View style={{
                    padding: wp('2%'),
                    flexDirection: "row",
                    borderBottomColor: COLORS.dark_grey,
                    borderBottomWidth: 0.5,
                    margin: wp('2%'),
                    justifyContent: 'space-between'
                }}>
                    <View style={{ flexDirection: 'row' }}>
                        <Icon name="adduser" size={25} color="black" />
                        <Text style={styles.memberText}>{STRINGS.CLASS_DETAILS_MEMBERS_TEXT}</Text>
                    </View>
                    <Icon name="right" size={25} color="black" />
                </View>
                <FlatList
                    horizontal
                    style={{ padding: wp('2%') }}
                    data={ListItems}
                    renderItem={this._renderImgList}
                    ListFooterComponent={this.footer}
                    ItemSeparatorComponent={this._itemSeparator}
                    extraData={this.state.refresh}
                    keyExtractor={(item, index) => index}
                />
            </ShadowViewContainer>
        )
    }
    // -------------------------------------------------------

    // =============================================================================================
    // Render method for recommend views
    // =============================================================================================
    _renderRecommend() {
        const { ListItems } = this.state;
        return (
            <ShadowViewContainer style={{ padding: wp('2%'), margin: wp('2%') }}>
                <View style={{
                    flexDirection: "row",
                    justifyContent: 'space-between'
                }}>
                    <View style={{ flexDirection: "row" }}>
                        <View style={{ width: wp('1%'), backgroundColor: "orange" }} />
                        <Spacer row={0.2} />
                        <Text style={styles.recommendText} >{STRINGS.CLASS_DETAILS_RECOMMEND_TEXT}</Text>
                    </View>
                    <View style={{ flexDirection: "row", }}>
                        <Text>{STRINGS.CLASS_DETAILS_SEEALL_TEXT}</Text>
                        <Spacer row={0.2} />
                        <Icon name="playcircleo" size={15} color="grey" />
                        <Spacer row={1.5} />
                    </View>
                </View>
                <Spacer space={1} />
                <FlatList
                    style={{ padding: wp('2%') }}
                    data={ListItems}
                    renderItem={this._renderRecommendList}
                    extraData={this.state.refresh}
                    keyExtractor={(item, index) => index}
                />
            </ShadowViewContainer>
        )
    }
    // -------------------------------------------------------

    // =============================================================================================
    // Render method for Listview
    // =============================================================================================


    _renderImgList = ({ item, index }) => (

        <View style={{
            width: wp('16%'),
            height: wp('18%')
        }} >
            <Image style={styles.imageCell}
                source={ICONS.USER_IMG} />
            <Text style={styles.listText} >{item.name}</Text>
        </View>

    );
    footer = () => {
        return (
            <View>
                <ImageBackground style={styles.footerCell} >
                    <Text style={styles.footerText} >+</Text>
                </ImageBackground>
                <Text style={styles.listText} >invite</Text>
            </View>

        )
    }
    _renderRecommendList = ({ item, index }) => (

        <View style={{
            borderRadius: wp('2%'), paddingHorizontal: wp('5%'),
            width: wp('90%'), alignSelf: 'center',
        }} >
            <ShadowViewContainer style={{ padding: wp('2%') }}>
                <View style={{
                    flexDirection: "row",
                    margin: wp('0.5%'),
                    justifyContent: 'space-between'
                }}>
                    <Image
                        style={styles.recommendImg}
                        source={ICONS.KINGDOM_IMG} />
                    <View style={{ width: wp('45%') }}>
                        <Text style={{ fontWeight: "500", color: 'black', fontSize: 16 }} >The Gaint Kingdom</Text>
                        <Spacer space={1} />
                        <Text style={{ color: 'grey', marginLeft: 5, alignSelf: "center" }} >{STRINGS.KINGDOM_TITLE}</Text>
                        <Spacer space={1} />
                        <View style={{
                            flexDirection: "row",
                            margin: wp('0.5%'),
                            justifyContent: 'space-between'
                        }}>
                            <View style={{
                                flexDirection: "row"
                            }}>
                                <Text style={{ color: 'red', alignSelf: "center" }} >265</Text>
                                <Text style={{ color: 'grey', marginLeft: 5, alignSelf: "center" }} >remaining</Text>
                            </View>
                            <View style={{
                                padding: wp('2%'),
                                paddingHorizontal: wp('5%'),
                                backgroundColor: 'orange',
                                borderBottomLeftRadius: 10,
                                borderBottomRightRadius: 10,
                                borderTopRightRadius: 10
                            }} >
                                <Text style={{ color: 'white', padding: 5 }} >241</Text>
                            </View>
                        </View>
                    </View>
                </View>

            </ShadowViewContainer>

        </View>

    );

    _itemSeparator = () => (<View style={{ width: wp('5%') }} />);

    // -------------------------------------------------------

    render() {
        console.log('test debug')
        return (
            <SafeAreaViewContainer>
                <DismissKeyboard>
                    <MainContainer style={styles.mainContainerStyle}>
                        <ImageBackground source={ICONS.CLASS_BG_IMG}
                            style={styles.image}
                            imageStyle={{
                                borderTopLeftRadius: wp('2%'),
                                borderTopRightRadius: wp('2%')
                            }}>
                            {/* <SafeAreaViewContainer> */}
                            {this._renderHeader()}
                            {/* </SafeAreaViewContainer> */}

                            <ScrollView>
                                <View style={styles.mainView}>
                                    <Spacer space={2} />
                                    <Image style={styles.userImage}
                                        source={ICONS.USER_IMG}
                                        imageStyle={{
                                            borderTopLeftRadius: wp('1%'),
                                            borderTopRightRadius: wp('1%')
                                        }} />
                                    <Spacer space={1} />
                                    <Text style={styles.userNameText}>Michael John</Text>
                                    <Spacer space={1} />
                                    <Text style={styles.classIdText} >{STRINGS.CLASS_DETAILS_CLASS_ID}</Text>
                                    <Spacer space={1} />
                                    {this._renderMembers()}
                                    <Spacer space={1} />
                                    {this._renderRecommend()}
                                </View>
                            </ScrollView>
                        </ImageBackground>
                    </MainContainer>
                </DismissKeyboard>

            </SafeAreaViewContainer>
        )
    }
}

export default ClassDetail;