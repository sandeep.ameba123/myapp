import React from 'react';
import styled from 'styled-components';
import COLORS from "../../utils/Colors";
import { FONT } from "../../utils/FontSizes";
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen'
import { StyleSheet } from 'react-native';
const styles = StyleSheet.create({

    mainContainerStyle: {
        flex: 1,
    },
    mainView: {
        padding:wp('3%'),
        marginTop:0 ,
        flex:1,
    },
    image: {
        resizeMode: "cover",
        height: hp('35%'),
        width: '100%',
        flex:1
    },
    userImage: {
        resizeMode: "cover",
        height: wp('20%'),
        width: wp('20%'),
        borderRadius: wp('20%') / 2,
        borderWidth: 0.5,
        borderColor: COLORS.white_color,
        alignSelf:"center",
    },
    recommendImg:{
        resizeMode: "cover",
        height: wp('35%'),
        width: wp('25%'),
    },
    imageCell: {
        resizeMode: "cover",
        height: wp('15%'),
        width: wp('15%'),
        borderRadius: wp('15%') / 2,
        borderWidth: 0.5,
        borderColor: COLORS.white_color,
        alignSelf:"center",
    },
    userNameText: {
        fontWeight: "600",
        color: COLORS.white_color,
        alignSelf:"center",
        fontSize:FONT.TextNormal,        
    },
    classIdText: {
        color: COLORS.white_color,
        alignSelf:"center",
        fontSize:FONT.TextSmall_2,        
    },
    footerCell: {
        height: wp('15%'),
        width: wp('15%'),
        borderRadius: wp('15%') / 2,
        borderWidth: 0.5,
        borderColor: COLORS.white_color,
        alignSelf:"center",
        backgroundColor:"orange",
        justifyContent:'center',
        marginLeft:wp('2%')
    },
    footerText: {
        fontWeight: "600",
        color: COLORS.white_color,
        alignSelf:"center",
        fontSize:FONT.TextNormal,        
    },
    recommendText:{
        fontWeight: "600",
        color: COLORS.black_color,
        alignSelf:"center",
        fontSize:FONT.TextSmall,  
    },
    listText: {       
        color: COLORS.black_color,
        alignSelf:"center",
        fontSize:FONT.TextExtraSmall,        
    },
    memberText: {       
        color: COLORS.black_color,
        alignSelf:"center",
        fontSize:FONT.TextSmall,
        marginLeft:5        
    },
})

export default styles;