import React from 'react';
import styled from 'styled-components';
import COLORS from "../../utils/Colors";
import { FONT } from "../../utils/FontSizes";
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen'
import { StyleSheet } from 'react-native';
const styles = StyleSheet.create({

    mainContainerStyle: {
        flex: 1,
    },
});

export default styles;