/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import React from "react";
import {
    View, Text, Image, Keyboard, StatusBar, ImageBackground,
    TouchableOpacity, TouchableWithoutFeedback, Platform, ScrollView,
    FlatList
} from "react-native";
import styles from "./styles";
import BaseClass from "../../utils/BaseClass";
import STRINGS from '../../utils/Strings'
import COLORS from "../../utils/Colors";
import { FONT } from "../../utils/FontSizes";
import {
    SafeAreaViewContainer, MainContainer, ShadowViewContainer,
    ScrollContainer
} from "../../utils/BaseStyle";
import { ICONS } from "../../utils/ImagePaths";
import { Spacer } from '../../components/Spacer/SpacerView'
// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import { Header } from 'react-native-elements';
import { widthPercentageToDP as wp } from 'react-native-responsive-screen'
import Icon from 'react-native-vector-icons/AntDesign';
Icon.loadFont()
import StickyParallaxHeader from 'react-native-sticky-parallax-header'

const DismissKeyboard = ({ children }) => (
    <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
        {children}
    </TouchableWithoutFeedback>
);
class Parallax extends BaseClass {

    constructor(props) {
        super(props);
        this.state = {
        }
    }
    render() {
        return (
            <StickyParallaxHeader headerType="TabbedHeader" />
        )
    }
}

export default Parallax;