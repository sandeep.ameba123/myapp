/**
 * @format
 */

import {AppRegistry} from 'react-native';
//import App from './App';
import App from './src/screens/ClassDetail';
// import App from './src/screens/ProfileDetail';
// import App from './src/screens/ParallaxScreen';
import {name as appName} from './app.json';

AppRegistry.registerComponent(appName, () => App);
